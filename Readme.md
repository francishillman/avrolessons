# Trying out some Avro

This repo contains some tests I did with Avro.

* **Avrotest** - Main implementation of different ways of encoding and decoding with Avro in Java
* **AvroBench** - Crude micro-benchmarks of Avrotest
* **PyAvro** - a failed attempt at reading the same bytes cleanly in Python