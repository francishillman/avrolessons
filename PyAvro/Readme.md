# Python testing
Just some attempts reading the data written in Java back in Python.

*fastavro* does have better api documentation but is a much smaller project
which only focusses on encoding/decoding avro files (with schema or checking against given schema).

Apache avro for python is slower and also seems to have problems with reading the exact same data
(I need to change some bytes if I want to read it in correctly)

