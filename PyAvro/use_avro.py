import io
import base64
import avro.schema
from avro.io import DatumReader, BinaryDecoder, DatumWriter, BinaryEncoder

schema = avro.schema.parse(open("bottle.avsc", "rb").read())
reader = DatumReader(schema)
writer = DatumWriter(schema)

bottle_dict = {"message_id": 100, "message_sender": "Jan", "message_type": "GLOAT", "message": "I love this beach!"}
bytes_writer = io.BytesIO()
prepared_encoder = BinaryEncoder(bytes_writer)
writer.write(bottle_dict, prepared_encoder)
raw_bytes = bytes_writer.getvalue()
print("Encoded in python:")
print(len(raw_bytes), raw_bytes)

# Where does the \x02 come from?
datum_enc_bytes = b'\x02' + base64.b64decode("AMgBAAZKYW4AJEkgbG92ZSB0aGlzIGJlYWNoIQI=")
print("Bytes read from Java datum-encoder:")
print(len(datum_enc_bytes), datum_enc_bytes)


datum_enc_bytesIO = io.BytesIO(datum_enc_bytes)
loaded_decoder = BinaryDecoder(datum_enc_bytesIO)
bottle_dict = reader.read(loaded_decoder)
print(bottle_dict)

object_bytes = base64.b64decode(b"wwGAJpoKpbzTjgDIAQAGSmFuACRJIGxvdmUgdGhpcyBiZWFjaCEC")
print(object_bytes)
# This is even weirder
object_bytes_slice = b'\x02' + object_bytes[10:]
print("Bytes read for Java object method:")
print(len(object_bytes_slice), object_bytes_slice)
object_bytesIO = io.BytesIO(object_bytes_slice)
loaded_decoder2 = BinaryDecoder(object_bytesIO)
bottle_dict2 = reader.read(loaded_decoder2)
print(bottle_dict2)