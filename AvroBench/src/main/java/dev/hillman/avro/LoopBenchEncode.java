package dev.hillman.avro;

import org.apache.commons.lang3.RandomStringUtils;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class LoopBenchEncode {

  @State(Scope.Benchmark)
  public static class DataSet {

    @Param({"100", "250", "500", "1000", "5000"})
    int message_count;
    boolean objectMethod;
    BottleMessage[] data;
    byte[][] bytes;

    DatumTransformer datumTransformer;

    @Setup
    public void buildDataset() {
      data = BenchRunner.buildDataset(message_count);
      bytes = new byte[message_count][];
    }

    @TearDown
    public void checkOutput() {
      for (int i = 0; i < message_count; i++) {
        BottleMessage outputMessage;
        if (objectMethod) {
          outputMessage = Main.objectMethodDecode(bytes[i]);
        } else {
          outputMessage = datumTransformer.decode(bytes[i]);
        }
        assert outputMessage.equals(data[i]) : "Objects should still be correct";
      }
    }
  }


  @Benchmark
  @BenchmarkMode(Mode.AverageTime)
  @Warmup(iterations = 3)
  @Measurement(iterations = 5)
  @OutputTimeUnit(TimeUnit.MILLISECONDS)
  public void testObjectMethod(DataSet set) {
    set.objectMethod = true;
    for (int i = 0; i < set.message_count; i++) {
      set.bytes[i] = Main.objectMethodEncode(set.data[i]);
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.AverageTime)
  @Warmup(iterations = 3)
  @Measurement(iterations = 5)
  @OutputTimeUnit(TimeUnit.MILLISECONDS)
  public void testDecoderMethod(DataSet set) {
    set.objectMethod = false;
    var bottleTransformer = new DatumTransformer();
    set.datumTransformer = bottleTransformer;
    for (int i = 0; i < set.message_count; i++) {
      set.bytes[i] = bottleTransformer.encode(set.data[i]);
    }
  }

}
