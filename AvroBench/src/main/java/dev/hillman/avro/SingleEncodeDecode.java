package dev.hillman.avro;

import org.apache.commons.lang3.RandomStringUtils;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
public class SingleEncodeDecode {

  DatumTransformer datumTransformer;

  BottleMessage changingRandomMessage;
  BottleMessage decodedMessage;

  @Setup
  public void buildSample() {
    datumTransformer = new DatumTransformer();
    Random random = new Random();
    BottleMessage.Builder bottleBuild = BottleMessage.newBuilder();
    changingRandomMessage = bottleBuild
        .setMessage(RandomStringUtils.random(32))
        .setMessageId(random.nextInt())
        .setMessageSender(RandomStringUtils.random(6, true, false))
        .setMessageType(MessageType.values()[random.nextInt(MessageType.values().length)])
        .build();
  }

  @TearDown
  public void checkOutput() {

  }

  @Benchmark
  public void testObjectMethod(SingleEncodeDecode single) {
    byte[] encoded = Main.objectMethodEncode(single.changingRandomMessage);
    single.decodedMessage = Main.objectMethodDecode(encoded);
  }

  @Benchmark
  public void testDecoderMethod(SingleEncodeDecode single) {
    byte[] encoded = datumTransformer.encode(single.changingRandomMessage);
    single.decodedMessage = datumTransformer.decode(encoded);
  }
}
