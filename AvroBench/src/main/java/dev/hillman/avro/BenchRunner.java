package dev.hillman.avro;

import org.apache.commons.lang3.RandomStringUtils;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Random;

public class BenchRunner {

  public static BottleMessage[] buildDataset(int messagesToTest) {
    Random random = new Random(123L);
    BottleMessage[] testArray = new BottleMessage[messagesToTest];
    BottleMessage.Builder bottleBuild;
    for (int i = 0; i < messagesToTest; i++) {
      bottleBuild = BottleMessage.newBuilder();
      testArray[i] = bottleBuild
          .setMessage(RandomStringUtils.random(32, 0, 0, true, true, null, random))
          .setMessageId(random.nextInt())
          .setMessageSender(RandomStringUtils.random(6, 0, 0, true, false, null, random))
          .setMessageType(MessageType.values()[random.nextInt(MessageType.values().length)])
          .build();
    }
    return testArray;
  }

  public static void main(String[] args) throws RunnerException {
    Options opt = new OptionsBuilder()
        .include(LoopEncodeDecode.class.getSimpleName())
        .forks(1)
        .jvmArgs("-ea")
        .build();

    new Runner(opt).run();
  }
}
