package dev.hillman.avro;

import org.openjdk.jmh.annotations.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.TimeUnit;

public class LoopEncodeDecode {

  @State(Scope.Benchmark)
  public static class DataSet {

    @Param({"100", "250", "500", "1000", "5000"})
    int message_count;

    BottleMessage[] data;
    BottleMessage[] dataOut;

    @Setup
    public void buildDataset() {
      data = BenchRunner.buildDataset(message_count);
    }

    @TearDown
    public void checkOutput() {
      for (int i = 0; i < message_count; i++) {
        assert dataOut[i].equals(data[i]) : "Objects should still be correct";
      }
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.AverageTime)
  @Warmup(iterations = 1)
  @Measurement(iterations = 3)
  @OutputTimeUnit(TimeUnit.MILLISECONDS)
  public void testObjectMethod(DataSet set) {
    set.dataOut = new BottleMessage[set.message_count];
    for (int i = 0; i < set.message_count; i++) {
      byte[] bytes = Main.objectMethodEncode(set.data[i]);
      set.dataOut[i] = Main.objectMethodDecode(bytes);
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.AverageTime)
  @Warmup(iterations = 1)
  @Measurement(iterations = 3)
  @OutputTimeUnit(TimeUnit.MILLISECONDS)
  public void testDecoderMethod(DataSet set) {
    var bottleTransformer = new DatumTransformer();
    set.dataOut = new BottleMessage[set.message_count];
    for (int i = 0; i < set.message_count; i++) {
      byte[] bytes = bottleTransformer.encode(set.data[i]);
      set.dataOut[i] = bottleTransformer.decode(bytes);
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.AverageTime)
  @Warmup(iterations = 1)
  @Measurement(iterations = 3)
  @OutputTimeUnit(TimeUnit.MILLISECONDS)
  public void testBulkDecoderMethod(DataSet set) {
    var bottleTransformer = new DatumTransformer();
    ByteArrayOutputStream outputStream = bottleTransformer.encodeMulti(set.data);
    ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
    set.dataOut = bottleTransformer.decodeMulti(inputStream, set.message_count);
  }


}
