# My Results

Putting the two methods head to head. Note that there is a bunch of object creation happening in
the testDecoderMethod which makes it more error prone (and slightly slower head to head).
```
Benchmark                              Mode  Cnt       Score       Error  Units
SingleEncodeDecode.testDecoderMethod  thrpt    5  826057.525 ± 74266.297  ops/s
SingleEncodeDecode.testObjectMethod   thrpt    5  785583.895 ± 13358.415  ops/s
```


Doing many encodings after each other, it doesn't suddenly show a much quicker method.
```
Benchmark                          (message_count)  Mode  Cnt  Score   Error  Units
LoopBenchEncode.testDecoderMethod              100  avgt    5  0.050 ± 0.001  ms/op
LoopBenchEncode.testDecoderMethod              250  avgt    5  0.124 ± 0.003  ms/op
LoopBenchEncode.testDecoderMethod              500  avgt    5  0.254 ± 0.005  ms/op
LoopBenchEncode.testDecoderMethod             1000  avgt    5  0.514 ± 0.054  ms/op
LoopBenchEncode.testDecoderMethod             5000  avgt    5  2.492 ± 0.145  ms/op
LoopBenchEncode.testObjectMethod               100  avgt    5  0.051 ± 0.002  ms/op
LoopBenchEncode.testObjectMethod               250  avgt    5  0.126 ± 0.004  ms/op
LoopBenchEncode.testObjectMethod               500  avgt    5  0.254 ± 0.006  ms/op
LoopBenchEncode.testObjectMethod              1000  avgt    5  0.490 ± 0.017  ms/op
LoopBenchEncode.testObjectMethod              5000  avgt    5  2.534 ± 0.152  ms/op
```


If we add decoding and a bulk method you'll see the bulk approach is a bit quicker and that is
even with copying the complete Stream.
```
Benchmark                               (message_count)  Mode  Cnt  Score   Error  Units
LoopEncodeDecode.testBulkDecoderMethod              100  avgt    3  0.084 ± 0.004  ms/op
LoopEncodeDecode.testBulkDecoderMethod              250  avgt    3  0.218 ± 0.163  ms/op
LoopEncodeDecode.testBulkDecoderMethod              500  avgt    3  0.444 ± 0.195  ms/op
LoopEncodeDecode.testBulkDecoderMethod             1000  avgt    3  0.835 ± 0.141  ms/op
LoopEncodeDecode.testBulkDecoderMethod             5000  avgt    3  4.182 ± 1.239  ms/op
LoopEncodeDecode.testDecoderMethod                  100  avgt    3  0.092 ± 0.035  ms/op
LoopEncodeDecode.testDecoderMethod                  250  avgt    3  0.262 ± 0.078  ms/op
LoopEncodeDecode.testDecoderMethod                  500  avgt    3  0.501 ± 0.079  ms/op
LoopEncodeDecode.testDecoderMethod                 1000  avgt    3  1.033 ± 0.631  ms/op
LoopEncodeDecode.testDecoderMethod                 5000  avgt    3  5.072 ± 6.678  ms/op
LoopEncodeDecode.testObjectMethod                   100  avgt    3  0.097 ± 0.042  ms/op
LoopEncodeDecode.testObjectMethod                   250  avgt    3  0.242 ± 0.010  ms/op
LoopEncodeDecode.testObjectMethod                   500  avgt    3  0.489 ± 0.046  ms/op
LoopEncodeDecode.testObjectMethod                  1000  avgt    3  0.990 ± 0.088  ms/op
LoopEncodeDecode.testObjectMethod                  5000  avgt    3  4.984 ± 1.000  ms/op
```