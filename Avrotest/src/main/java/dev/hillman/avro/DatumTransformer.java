package dev.hillman.avro;

import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DatumTransformer {

  DatumWriter<BottleMessage> bottleMessageDatumWriter =
      new SpecificDatumWriter<>(BottleMessage.class);
  DatumReader<BottleMessage> bottleMessageDatumReader =
      new SpecificDatumReader<>(BottleMessage.class);
  EncoderFactory encoderFactory = EncoderFactory.get();
  DecoderFactory decoderFactory = DecoderFactory.get();

  BinaryEncoder reuseEncoder = null;
  BinaryDecoder reuseDecoder = null;

  public byte[] encode(BottleMessage bottleMessage) {
    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    reuseEncoder = encoderFactory.binaryEncoder(byteStream, reuseEncoder);
    try {
      bottleMessageDatumWriter.write(bottleMessage, reuseEncoder);
      reuseEncoder.flush();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
    return byteStream.toByteArray();
  }

  public BottleMessage decode(byte[] bytes) {
    reuseDecoder = decoderFactory.binaryDecoder(bytes, reuseDecoder);
    try {
      // Reuse variable not used in this example
      return bottleMessageDatumReader.read(null, reuseDecoder);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  public ByteArrayOutputStream encodeMulti(BottleMessage[] bottleMessages) {
    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    reuseEncoder = encoderFactory.binaryEncoder(byteStream, reuseEncoder);
    try {
      for (BottleMessage bottleMessage : bottleMessages) {
        bottleMessageDatumWriter.write(bottleMessage, reuseEncoder);
      }
      reuseEncoder.flush();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
    return byteStream;
  }

  public BottleMessage[] decodeMulti(InputStream dataStream, int messages) {
    reuseDecoder = decoderFactory.binaryDecoder(dataStream, reuseDecoder);
    try {
      BottleMessage[] outMessages = new BottleMessage[messages];
      for (int i = 0; i < messages; i++) {
        outMessages[i] = bottleMessageDatumReader.read(null, reuseDecoder);
      }
      return outMessages;
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }
}
