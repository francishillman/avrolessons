package dev.hillman.avro;

import java.util.Objects;

public class BottleShip {
  private String description;

  public BottleShip() {}

  public BottleShip(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public BottleShip setDescription(String description) {
    this.description = description;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BottleShip that = (BottleShip) o;
    return description.equals(that.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(description);
  }
}
