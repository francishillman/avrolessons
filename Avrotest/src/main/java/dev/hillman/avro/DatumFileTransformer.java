package dev.hillman.avro;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class DatumFileTransformer {

  SpecificDatumWriter<BottleMessage> bottleMessageDatumWriter =
      new SpecificDatumWriter<>(BottleMessage.class);
  DatumReader<BottleMessage> bottleMessageDatumReader =
      new SpecificDatumReader<>(BottleMessage.class);
  DataFileWriter<BottleMessage> bottleFileWriter = new DataFileWriter<>(bottleMessageDatumWriter);
  DataFileReader<BottleMessage> bottleFileReader;

  public File encode(BottleMessage bottleMessage) {
    try {
      File outfile = new File("./bottleMessage.avro");

      bottleFileWriter.create(BottleMessage.getClassSchema(), outfile);
      bottleFileWriter.append(bottleMessage);
      bottleFileWriter.close();
      return outfile;
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  public BottleMessage decode(File bottleFile) {
    try {
      bottleFileReader = new DataFileReader<BottleMessage>(bottleFile, bottleMessageDatumReader);
      BottleMessage message = bottleFileReader.next();
      bottleFileReader.close();
      return message;
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }
}
