package dev.hillman.avro;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;

import java.io.File;
import java.io.IOException;

public class GenericAvro {

  public static GenericRecord genericFileReader(File file) {
    GenericDatumReader<GenericRecord> reader = new GenericDatumReader<>();
    try {
      DataFileReader<GenericRecord> fileReader = new DataFileReader<GenericRecord>(file, reader);
      GenericRecord record = fileReader.next();
      fileReader.close();
      return record;
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }
}
