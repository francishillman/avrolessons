package dev.hillman.avro;

import org.apache.avro.io.*;
import org.apache.avro.reflect.ReflectDatumReader;
import org.apache.avro.reflect.ReflectDatumWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ReflectionTransformer {
  DatumWriter<BottleShip> bottleShipDatumWriter =
      new ReflectDatumWriter<>(BottleShip.class);
  DatumReader<BottleShip> bottleShipDatumReader =
      new ReflectDatumReader<>(BottleShip.class);
  EncoderFactory encoderFactory = EncoderFactory.get();
  DecoderFactory decoderFactory = DecoderFactory.get();

  BinaryEncoder reuseEncoder = null;
  BinaryDecoder reuseDecoder = null;

  public byte[] encode(BottleShip bottleMessage) {
    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    reuseEncoder = encoderFactory.binaryEncoder(byteStream, reuseEncoder);
    try {
      bottleShipDatumWriter.write(bottleMessage, reuseEncoder);
      reuseEncoder.flush();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
    return byteStream.toByteArray();
  }

  public BottleShip decode(byte[] bytes) {
    reuseDecoder = decoderFactory.binaryDecoder(bytes, reuseDecoder);
    try {
      // Reuse variable not used in this example
      return bottleShipDatumReader.read(null, reuseDecoder);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }
}
