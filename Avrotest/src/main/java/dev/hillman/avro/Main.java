package dev.hillman.avro;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;

public class Main {
  public static BottleMessage createBottleMessage() {
    BottleMessage.Builder bottleBuild = BottleMessage.newBuilder();
    return bottleBuild
        .setMessage("I love this beach!")
        .setMessageId(100)
        .setMessageSender("Jan")
        .setMessageType(MessageType.GLOAT)
        .build();
  }

  public static BottleShip createBottleShip() {
    BottleShip ship = new BottleShip("A tiny ship");
    return ship;
  }

  public static BottleMessage[] createFewBottleMessages() {
    BottleMessage[] messages = new BottleMessage[3];
    BottleMessage.Builder bottleBuild = BottleMessage.newBuilder();
    messages[0] = bottleBuild
        .setMessage("I love this beach!")
        .setMessageId(100)
        .setMessageSender("Jan")
        .setMessageType(MessageType.GLOAT)
        .build();
    bottleBuild = BottleMessage.newBuilder();
    messages[1] = bottleBuild
        .setMessage("Whelp, where am I ?!?")
        .setMessageId(150)
        .setMessageSender("Piet")
        .setMessageType(MessageType.HELP)
        .build();
    bottleBuild = BottleMessage.newBuilder();
    messages[2] = bottleBuild
        .setMessage("Did you know you can eat these Cherries")
        .setMessageId(42)
        .setMessageSender("Henk")
        .setMessageType(MessageType.SHARE)
        .build();
    return messages;
  }

  public static byte[] objectMethodEncode(BottleMessage bottleMessage) {
    try {
      ByteBuffer buffer = bottleMessage.toByteBuffer();
      return buffer.array();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  public static BottleMessage objectMethodDecode(byte[] bytes) {
    try {
      return BottleMessage.fromByteBuffer(ByteBuffer.wrap(bytes));
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  public static void main(String[] args) {
    BottleMessage bottleMessage = createBottleMessage();
    Base64.Encoder enc = Base64.getEncoder();

    byte[] objectBytes = objectMethodEncode(bottleMessage);
    System.out.println("Obj: "+ enc.encodeToString(objectBytes));

    DatumTransformer transformer = new DatumTransformer();
    byte[] datumEncoderBytes = transformer.encode(bottleMessage);
    System.out.println("datumEnc: "+ enc.encodeToString(datumEncoderBytes));
  }
}
