package dev.hillman.avro;


import org.apache.avro.generic.GenericRecord;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TwoWayTests {

  @Test
  public void objectMethodTest() {
    BottleMessage bottleMessage = Main.createBottleMessage();
    byte[] somebytes = Main.objectMethodEncode(bottleMessage);
    BottleMessage otherBottle = Main.objectMethodDecode(somebytes);
    assertEquals(bottleMessage, otherBottle);
    System.out.println(bottleMessage);
  }

  @Test
  public void decoderMethodTest() {
    var bottleTransformer = new DatumTransformer();

    BottleMessage bottleMessage = Main.createBottleMessage();
    byte[] somebytes = bottleTransformer.encode(bottleMessage);
    BottleMessage otherBottle = bottleTransformer.decode(somebytes);
    assertEquals(bottleMessage, otherBottle);
  }

  @Test
  public void frankensteinCombination() {
    var bottleTransformer = new DatumTransformer();
    BottleMessage bottleMessage = Main.createBottleMessage();
    byte[] somebytes = Main.objectMethodEncode(bottleMessage);
    // Chop off first 10 bytes
    byte[] pureObjectBytes = Arrays.copyOfRange(somebytes, 10, somebytes.length);

    BottleMessage otherBottle = bottleTransformer.decode(pureObjectBytes);
    assertEquals(bottleMessage, otherBottle);
  }

  @Test
  public void fileSerialization() {
    var bottleTransformer = new DatumFileTransformer();
    BottleMessage bottleMessage = Main.createBottleMessage();
    File saveFile = bottleTransformer.encode(bottleMessage);

    BottleMessage otherBottle = bottleTransformer.decode(saveFile);
    assertEquals(bottleMessage, otherBottle);

    saveFile.delete();
  }

  @Test
  public void genericFileReading() {
    var bottleTransformer = new DatumFileTransformer();
    BottleMessage bottleMessage = Main.createBottleMessage();
    File saveFile = bottleTransformer.encode(bottleMessage);

    // We can even read out the files without knowing the schema (it's included)
    GenericRecord genericData = GenericAvro.genericFileReader(saveFile);
    int message_id = (int) genericData.get("message_id");
    assertEquals(bottleMessage.getMessageId(), message_id);

    saveFile.delete();
  }

  @Test
  public void decoderMethodMultiObjectTest() {
    var bottleTransformer = new DatumTransformer();

    BottleMessage[] bottleMessages = Main.createFewBottleMessages();
    int bottleMessageCount = bottleMessages.length;
    ByteArrayOutputStream outputStream = bottleTransformer.encodeMulti(bottleMessages);

    ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
    BottleMessage[] otherBottles = bottleTransformer.decodeMulti(inputStream, bottleMessageCount);

    for (int i = 0; i < bottleMessageCount; i++) {
      assertEquals(bottleMessages[i], otherBottles[i]);
    }
  }

  @Test
  public void reflectDecoderMethodTest() {
    var bottleTransformer = new ReflectionTransformer();

    BottleShip bottle = Main.createBottleShip();
    byte[] somebytes = bottleTransformer.encode(bottle);
    BottleShip otherBottle = bottleTransformer.decode(somebytes);
    assertEquals(bottle, otherBottle);
  }
}
