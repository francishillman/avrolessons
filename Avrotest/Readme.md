# Avro methods

This project shows some different avro methods of saving.
Use `TwoWayTests` as a guide for the different implementations.

## Links

* [baeldung](https://www.baeldung.com/java-apache-avro)
* [Avro serialization methods gist](https://gist.github.com/davideicardi/e8c5a69b98e2a0f18867b637069d03a9)
* [Avro current spec](https://avro.apache.org/docs/current/spec.html#Data+Serialization+and+Deserialization)
* [Avro example with service & protocol](https://github.com/alexholmes/avro-maven)
* [Some blog](https://ouyi.github.io/post/2016/10/13/avro-binary-encoding.html)